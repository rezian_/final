<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use DB; 

class BookController extends Controller
{
    
    public function index()
    {
        $buku = Book::all();
        return view('book.index', compact('buku'));
    }

    
    public function create()
    {
        return view('book.create');
    }

 
    public function store(Request $request)
    {
        $request->validate([
            "judul"=>'required|unique:buku',
            "jumlah_halaman"=>'required',
            "tahun_terbit"=>'required',
            "description"=>'required'
        ]);

        $buku = Book::create([
            "judul" => $request['judul'],
            "jumlah_halaman" => $request['jumlah_halaman'],
            "tahun_terbit" => $request['tahun_terbit'],
            "description" => $request['description']
        ]);

        return redirect('/book')->with('success','Buku berhasil disimpan!');
    }

   
    public function show($id)
    {
        $buku=Book::find($id);
        return view('book.show', compact('buku'));
    }


    public function edit($id)
    {
        $buku = Book::find($id);
        return view('book.edit', compact('buku'));
    }

  
    public function update(Request $request, $id)
    {
        $request->validate([
            "judul"=>'required',
            "jumlah_halaman"=>'required',
            "tahun_terbit"=>'required',
            "description"=>'required'
            ]);

            $buku= Book::where('id', $id)->update([
                'judul' => $request['judul'],
                'jumlah_halaman' =>$request['jumlah_halaman'],
                'tahun_terbit' =>$request['tahun_terbit'],
                'description' =>$request['description']
            ]);
            return redirect('/book')->with('success','Berhasil update buku!');
    }


    public function destroy($id)
    {
        Book::destroy($id);
        return redirect('/book')->with('success','Buku Berhasil dihapus!');
    }
}
